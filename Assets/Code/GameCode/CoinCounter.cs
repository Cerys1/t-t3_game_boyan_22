using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCounter : MonoBehaviour
{
    public static int coinCount = 0;
    Text coinCountText;

    void Start()
    {
        //Finds the UI component that shows the coin count
        coinCountText = transform.GetComponent<Text>();
    }

    void Update()
    {
        //Updates the coin count on the UI
        coinCountText.text = coinCount.ToString();
    }
}
