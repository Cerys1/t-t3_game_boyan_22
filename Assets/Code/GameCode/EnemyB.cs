using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyB : MonoBehaviour
{
    public float enemyBSpeed = 0.05f;
    Transform point1;
    Transform point2;

    void Start()
    {
        //Finds the colliders that limit the patrol path 
        point1 = transform.parent.GetChild(1);
        point2 = transform.parent.GetChild(2);
    }

    void FixedUpdate()
    {
        //Moves the enemy by its speed
        transform.Translate(new Vector2(-enemyBSpeed, 0f));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision != null)
        {
            //Reverses the enemy's speed whenever it hits one of the path colliders
            if (collision.gameObject.transform == point1 || collision.gameObject.transform == point2)
            {
                enemyBSpeed = -1 * enemyBSpeed;
            }
        }
      
    }
}
