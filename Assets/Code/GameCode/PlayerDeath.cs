using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    public float inviTime = 0.5f;
    float timeLastHit = 0;
    bool playerHit = false;
    public static bool touchingGround;
    public static bool onOneWayGround;

    private void FixedUpdate()
    {
        //Checks if the player has been hit
        if(playerHit)
        {      
            //Tracks when the player was last hit and turns off the invincibility after it runs out
            if(timeLastHit >= inviTime)
            {
                playerHit = false;
            }
            else
            {
                timeLastHit += Time.fixedDeltaTime;
                Debug.Log(timeLastHit);
            }        
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Checks if the player has hit an enemy and doesn't have invincibility time
        if(collision.gameObject.CompareTag("Enemy") && playerHit == false)
        {
            //If player has no upgrades they die
            if (PlayerController.UpgradeState == 0)
            {
                Debug.Log("Player was hit by " + collision.gameObject);
                PlayerDied();
            }

            //If player has upgrades, they lose them and get some invincibility time
            else if(PlayerController.UpgradeState == 1 || PlayerController.UpgradeState == 2)
            {
                Debug.Log("Player was hit by " + collision.gameObject);
                PlayerController.UpgradeState = 0;
                playerHit = true;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        //Checks if the player is touching a platform or a one way platform
        if (collision.gameObject.CompareTag("Ground"))
        {
            touchingGround = true;
        }
        else if(collision.gameObject.CompareTag("OneWayGround"))
        {
            onOneWayGround = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //Tracks when the player stops touching a platform or a one way platform
        if (collision.gameObject.CompareTag("Ground"))
        {
            touchingGround = false;
        }
        else if (collision.gameObject.CompareTag("OneWayGround"))
        {
            onOneWayGround = false;
        }
    }

    //Sequence, triggered whent he player dies
    public static void PlayerDied()
    {
        //Reloads the current scene and resets the player's coins and upgrades
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        CoinCounter.coinCount = 0;
        PlayerController.UpgradeState = 0;
    }
}
