using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditTimer : MonoBehaviour
{
    float Timer = 0;

    void FixedUpdate()
    {
        //Counts seconds since the scene was opened
        Timer += Time.fixedDeltaTime;

        //Loads the main menu after 10 seconds have passed
        if(Timer >= 10)
        {
            LevelGoal.ReturnToMenu();
        }
    }
}
