using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D PlayerRigid;
    Transform PlayerCube;
    public float jumpHeight = 1;
    public float movementSpeed = 0.01f;
    public float slamSpeed = 2;
    public static int UpgradeState = 0;
    public GameObject GroundSlamAoE;
    bool slamming = false;
    public static KeyCode Left;
    public static KeyCode Right;
    public static KeyCode Down;
    public static KeyCode Sprint;
    public static KeyCode Jump;
    public static KeyCode Use;

    void Start()
    {
        //Saves the player object and its rigidbody
        PlayerRigid = transform.GetComponentInChildren<Rigidbody2D>();
        PlayerCube = transform.GetChild(0);
    }

    void Update()
    {
        //Jumps if player is touching the ground
        if (Input.GetKeyDown(Jump) && (PlayerDeath.touchingGround == true || PlayerDeath.onOneWayGround == true))
        {
            Debug.Log("jumping" + PlayerRigid);
            PlayerRigid.AddForce(transform.up * jumpHeight, ForceMode2D.Impulse);
        }

        //Checks if player presses Use
        if (Input.GetKeyDown(Use))
        {
            //Player shoots if they have upgrade A
            if (UpgradeState == 1)
            {
                ShootFireball.Shoot(PlayerCube);
            }

            //Player slams if they have upgrade B
            if (UpgradeState == 2)
            {
                GroundSlam();
            }
        }

        //Checks if the player has slammed into the gorund
        if (slamming == true && (PlayerDeath.touchingGround == true || PlayerDeath.onOneWayGround == true))
        {
            //Activates the ground slam AoE
            GroundSlamAoE.SetActive(true);

            //Sets that the player has finished slamming
            slamming = false;
        }

        //Checks if the player has pressed Down
        if (Input.GetKeyDown(Down))
        {
            //Checks if the player is standing on a one way platform
            if(PlayerDeath.onOneWayGround == true)
            {
                //Moves the player under the one way platform
                PlayerCube.Translate(new Vector2(0, -1.7f));
            }
            Debug.Log("Dropping down " + PlayerDeath.onOneWayGround);
        }

        //Returns the player to the main menu if they press Escape
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            LevelGoal.ReturnToMenu();
        }

    }
    private void FixedUpdate()
    {
        //Moves the player left and right, faster if they are holding Shift
        if (Input.GetKey(Left))
        {
            if (Input.GetKey(Sprint))
            {
                transform.Translate(new Vector2(-movementSpeed * 1.5f, 0f));
            }
            else
            {
                transform.Translate(new Vector2(-movementSpeed, 0f));
            }
        }

        if (Input.GetKey(Right))
        {
            if (Input.GetKey(Sprint))
            {
                transform.Translate(new Vector2(movementSpeed * 1.5f, 0f));
            }
            else
            {
                transform.Translate(new Vector2(movementSpeed, 0f));
            }
        }

        //Check if player has any upgrades
        if (UpgradeState != 0)
        {
            //If the player has any upgrades, increases the player model size and moves it according to the parent object so it remains in the same place visualy
            PlayerCube.localScale = new Vector3(1.5f, 1.5f, 1);
            PlayerCube.GetChild(0).localPosition = new Vector3(0, 1, -10);
        }
        else
        {
            //Resets the player model size and location relative to the parent if they have no upgrades
            PlayerCube.localScale = new Vector3(1, 1, 1);
            PlayerCube.GetChild(0).localPosition = new Vector3(0, 1.5f, -10);
        }   
    }

    //Sequence triggered when the player presses E while having the correct upgrade
    void GroundSlam()
    {
        Debug.Log(PlayerDeath.touchingGround);
        //Checks if the player is in the air
        if (PlayerDeath.touchingGround == false && PlayerDeath.onOneWayGround == false)
        {
            //Launches the player downwards and sets that the player has slammed
            PlayerRigid.AddForce(-transform.up * slamSpeed, ForceMode2D.Impulse);
            slamming = true;
        }
    }
}
