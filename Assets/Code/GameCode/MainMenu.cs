using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class MainMenu : MonoBehaviour
{
    Button playButton;
    Button controlsButton;
    Button quitButton;
    Button backButton;
    GameObject mainMenu;
    GameObject controlMenu;
    bool inMainMenu;
    bool listeningForKey;
    int keyIndex;
    Text[] bindText;

    Button leftBind;
    Button rightBind;
    Button downBind;
    Button jumpBind;
    Button sprintBind;
    Button useBind;
    
    void Start()
    {
        //Find the two separate canvases for later use
        mainMenu = transform.GetChild(0).gameObject;
        controlMenu = transform.GetChild(1).gameObject;

        //Adds listeners for the main menu buttons
        playButton = mainMenu.transform.GetChild(0).GetComponent<Button>();
        playButton.onClick.AddListener(OnPlayClick);

        controlsButton = mainMenu.transform.GetChild(1).GetComponent<Button>();
        controlsButton.onClick.AddListener(ChangeMenu);

        backButton = controlMenu.transform.GetChild(0).GetComponent<Button>();
        backButton.onClick.AddListener(ChangeMenu);

        quitButton = mainMenu.transform.GetChild(2).GetComponent<Button>();
        quitButton.onClick.AddListener(OnQuitClick);

        //Adding listeners for the control binding buttons
        leftBind = GameObject.Find("LeftBind").GetComponent<Button>();
        leftBind.onClick.AddListener(BindKeyLeft);

        rightBind = GameObject.Find("RightBind").GetComponent<Button>();
        rightBind.onClick.AddListener(BindKeyRight);

        downBind = GameObject.Find("DownBind").GetComponent<Button>();
        downBind.onClick.AddListener(BindKeyDown);

        jumpBind = GameObject.Find("JumpBind").GetComponent<Button>();
        jumpBind.onClick.AddListener(BindKeyJump);

        sprintBind = GameObject.Find("SprintBind").GetComponent<Button>();
        sprintBind.onClick.AddListener(BindKeySprint);

        useBind = GameObject.Find("UseBind").GetComponent<Button>();
        useBind.onClick.AddListener(BindKeyUse);

        //Deactivate the controls menu so that its not visible
        controlMenu.SetActive(false);
        inMainMenu = true;

        //Setting the default controls for the game
        PlayerController.Left = KeyCode.A;
        PlayerController.Right = KeyCode.D;
        PlayerController.Down = KeyCode.S;
        PlayerController.Jump = KeyCode.Space;
        PlayerController.Sprint = KeyCode.LeftShift;
        PlayerController.Use = KeyCode.E;

        //Assigning the text fields for all key binds
        bindText = new Text[6];
        for (int i = 0; i < 6; i++)
        {
            bindText[i] = controlMenu.transform.GetChild(2).GetChild(i).GetComponentInChildren<Text>();
        }
    }

    private void Update()
    {
        //Goes through all key bind text fields
        for(int i = 0; i < 6; i++)
        {
            if (bindText[i] != null)
            {
                //Checks if any key binds are conflicting and colours them red
                for (int j = 0; j < 6; j++)
                {
                    if (bindText[i].text == bindText[j].text && i != j)
                    {
                        bindText[i].color = Color.red;
                        bindText[j].color = Color.red;
                    }
                    else
                    {
                        bindText[i].color = Color.black;
                    }
                }

                //Updates the key for the binding that is being listened for
                if (i == 0)
                {
                    bindText[i].text = PlayerController.Left.ToString();
                }
                else if (i == 1)
                {
                    bindText[i].text = PlayerController.Right.ToString();
                }
                else if (i == 2)
                {
                    bindText[i].text = PlayerController.Down.ToString();
                }
                else if (i == 3)
                {
                    bindText[i].text = PlayerController.Jump.ToString();
                }
                else if (i == 4)
                {
                    bindText[i].text = PlayerController.Sprint.ToString();
                }
                else if (i == 5)
                {
                    bindText[i].text = PlayerController.Use.ToString();
                }
            }
        }


        //Assigns pressed key to the keybind the game was listening for
        if(listeningForKey)
        {
            //I check for all keys and see if one is pressed
            foreach (KeyCode kCode in Enum.GetValues(typeof(KeyCode)))
            {
                //As soon as a key has been detected as pressed its assigned to the bind that was being listend for and the listening ends
                if (Input.GetKey(kCode) && keyIndex == 0)
                {
                    PlayerController.Left = kCode;
                    listeningForKey = false;
                }
                else if (Input.GetKey(kCode) && keyIndex == 1)
                {
                    PlayerController.Right = kCode;
                    listeningForKey = false;
                }
                else if (Input.GetKey(kCode) && keyIndex == 2)
                {
                    PlayerController.Down = kCode;
                    listeningForKey = false;
                }
                else if (Input.GetKey(kCode) && keyIndex == 3)
                {
                    PlayerController.Jump = kCode;
                    listeningForKey = false;
                }
                else if (Input.GetKey(kCode) && keyIndex == 4)
                {
                    PlayerController.Sprint = kCode;
                    listeningForKey = false;
                }
                else if (Input.GetKey(kCode) && keyIndex == 5)
                {
                    PlayerController.Use = kCode;
                    listeningForKey = false;
                }
            }
        }
    }

    //Loading level 1
    void OnPlayClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Debug.Log("Loading Level 1");
    }

    //Going to the Controls menu
    void ChangeMenu()
    {
        inMainMenu = !inMainMenu;
        mainMenu.SetActive(inMainMenu);
        controlMenu.SetActive(!inMainMenu);
    }

    //Starts listening for a kCode for specific key bind
    void BindKeyLeft()
    {
        keyIndex = 0;
        listeningForKey = true;
    }
    void BindKeyRight()
    {
        keyIndex = 1;
        listeningForKey = true;
    }
    void BindKeyDown()
    {
        keyIndex = 2;
        listeningForKey = true;
    }
    void BindKeyJump()
    {
        keyIndex = 3;
        listeningForKey = true;
    }
    void BindKeySprint()
    {
        keyIndex = 4;
        listeningForKey = true;
    }
    void BindKeyUse()
    {
        keyIndex = 5;
        listeningForKey = true;
    }

    //Closing the game
    void OnQuitClick()
    {
        Application.Quit();
    }
}
