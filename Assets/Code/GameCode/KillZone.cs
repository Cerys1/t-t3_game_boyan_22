using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Logs which object has touched the killzone
        Debug.Log(collision.gameObject);

        //Checks if the object that touched the killzone is the player or not
        //Destroys any non-player object that touches the killzone
        if (!collision.CompareTag("Player"))
        {
            
            Destroy(collision.gameObject);
        }

        //Triggers the player death sequence if the player touches the killzone
        else if (collision.CompareTag("Player"))
        {         
            PlayerDeath.PlayerDied();
        }
    }
}
