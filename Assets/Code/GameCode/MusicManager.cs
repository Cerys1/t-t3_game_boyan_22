using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    AudioSource[] tracks;
    bool playingUpgradeATrack;
    bool playingUpgradeBTrack;

    void Start()
    {
        //Assigns all the music tracks for later use
        tracks = GetComponents<AudioSource>();
        tracks[0].Play();
        playingUpgradeATrack = false;
        playingUpgradeBTrack = false;
    }

    void Update()
    {
        //Plays certain music tracks when the player moves left or right
        if (PlayerController.UpgradeState == 1)
        {
            if(playingUpgradeATrack == false)
            {
                playingUpgradeATrack = true;
                Debug.LogWarning("playing Hihat " + PlayerController.UpgradeState);
                tracks[2].Play();
            }          
        }
        else
        {
            playingUpgradeATrack = false;
            tracks[2].Stop();
        }

        if (PlayerController.UpgradeState == 2)
        {
            if (playingUpgradeBTrack == false)
            {
                playingUpgradeBTrack = true;
                Debug.LogWarning("playing Snare " + PlayerController.UpgradeState);
                tracks[3].Play();
            }       
        }
        else
        {
            playingUpgradeBTrack = false;
            tracks[3].Stop();
        }
    }

    //Starts playing a track when theres an enemy nearby
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Enemy"))
        {
            tracks[1].Play();
        }
    }

    //Stops playing the track when the enemy leaves the area
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Enemy"))
        {
            tracks[1].Stop();
        }
    }
}
