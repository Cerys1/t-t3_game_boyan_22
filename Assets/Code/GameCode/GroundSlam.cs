using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSlam : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Checks if the AoE has hit any enemies
        if(collision.gameObject.CompareTag("Enemy"))
        {
            //Deletes any Enemy A that were hit by the AoE, then turns itself off
            if (collision.transform.parent.CompareTag("EnemyA"))
            {
                Destroy(collision.gameObject);
                transform.gameObject.SetActive(false);
            }            
        }
    }
}
