using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballFly : MonoBehaviour
{
    float flySpeed = 0.16f;
    float maxLife = 2f;
    float lifetime = 0;

    void FixedUpdate()
    {
        //Checks if the fireball's life time has reached the max
        if(lifetime <= maxLife)
        {
            //Moves the fireball and tracks how long it has existed
            transform.Translate(new Vector2(flySpeed, 0f));
            lifetime += Time.fixedDeltaTime;
        }
        else
        {
            //Destroys the fireball when it expires
            Destroy(transform.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Destoys any enemy the fireball touches, then it destroys itself
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            Destroy(transform.gameObject);
        }
    }
}
