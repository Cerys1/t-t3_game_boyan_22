using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCollect : MonoBehaviour
{
    public int coinValue = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Checks if the player has touched the coin
        if(collision.tag == "Player")
        {
            //Increases the total coin score by the value assigned to the specific coin
            CoinCounter.coinCount += coinValue;

            //Deactivates the coin object so this coin can no longer be collected
            transform.parent.gameObject.SetActive(false);
        }
    }
}
