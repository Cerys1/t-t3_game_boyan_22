using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFireball : MonoBehaviour
{
    public float fireRate = 2f;
    float lastFired = 0;
    static bool canFire = true;
    static GameObject fireball;
    public GameObject fireballPrefab;

    void Start()
    {
        //Takes the fireball object assigned in the Editor
        fireball = fireballPrefab;
    }

    void FixedUpdate()
    {
        //Cooldown until player can shoot again
        if(canFire == false)
        {
            if (lastFired >= fireRate)
            {
                canFire = true;
            }
            else
            {
                lastFired += Time.fixedDeltaTime;
            }
        }
        //Resets time when cooldown has ended
        else
        {
            lastFired = 0;
        }
        
    }

    //Sequence trigered when the player tries to shoot
    public static void Shoot(Transform Player)
    {
        //Checks if the shooting is out of cooldown
        if(canFire == true)
        {
            //Spawns a fireball object right in front of the player and starts the cooldown
            GameObject.Instantiate(fireball, new Vector3(Player.position.x + 1f, Player.position.y), new Quaternion());
            canFire = false;
        }
    }
}
