using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour
{
    Text UpgradeType;

    void Start()
    {
        //Gets the text component in the UI
        UpgradeType = transform.GetComponent<Text>();
    }

    void Update()
    {
        //Changes the text in the UI according to the upgrade state of the player
        if(PlayerController.UpgradeState == 0)
        {
            UpgradeType.text = "Type: None";
        }
        else if (PlayerController.UpgradeState == 1)
        {
            UpgradeType.text = "Type: Fire";
        }
        else if (PlayerController.UpgradeState == 2)
        {
            UpgradeType.text = "Type: Ground";
        }
    }
}
