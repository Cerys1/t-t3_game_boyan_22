using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGoal : MonoBehaviour
{
    int currentLevelNumber;

    void Start()
    {
        //Finds the build index of the current level
        currentLevelNumber = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(currentLevelNumber);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Checks if the player has reached the goal
        if(collision.gameObject.CompareTag("Player"))
        {
            //Loads the next level in the build order if the current one is not the last one
            Debug.Log("Loading next level: " + (currentLevelNumber + 1));
            if (currentLevelNumber < SceneManager.sceneCountInBuildSettings)
            {
                SceneManager.LoadScene(currentLevelNumber + 1);
            }
        }      
    }

    public static void ReturnToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
