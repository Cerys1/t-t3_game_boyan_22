using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAHead : MonoBehaviour
{
    GameObject Enemy;

    void Start()
    {
        //Gets the parent game object of the enemy it belongs to
        Enemy = transform.parent.parent.gameObject;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Destroys the enemy it belongs to when the player touches it
        if(collision.gameObject.CompareTag("Player"))
        {
            Destroy(Enemy);
        }
    }
}
