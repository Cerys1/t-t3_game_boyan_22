using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyA : MonoBehaviour
{
    public float enemyASpeed = 0.05f;

    void FixedUpdate()
    {
        //Moves the enemy
        transform.Translate(new Vector2(-enemyASpeed, 0f));             
    }
}
