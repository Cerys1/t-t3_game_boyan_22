using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeGot : MonoBehaviour
{
    public int UpgradeIndex = 1;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Checks if the player has touched the upgrade
        if (collision.CompareTag("Player"))
        {
            //Gives the player the specific upgrade index assigned to this upgrade collectible
            PlayerController.UpgradeState = UpgradeIndex;
            Debug.Log("Player Upgrade State: " + PlayerController.UpgradeState);
            transform.parent.gameObject.SetActive(false);
        }
    }
}