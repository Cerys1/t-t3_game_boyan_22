using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class9 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject WhiteTile = GameObject.Find("White");
        GameObject BlackTile = GameObject.Find("Black");
        GameObject[,] chessboard = new GameObject[8, 8];
        for (int y = 0; y < 8; y++)
        {
            for (int x = 0; x < 8; x++)
            {
                if ((y % 2 == 0 && x % 2 == 0) || (y % 2 == 1 && x % 2 == 1))
                {
                    chessboard[x, y] = Instantiate(WhiteTile, new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                }
                else if ((y % 2 == 0 && x % 2 == 1) || (y % 2 == 1 && x % 2 == 0))
                {
                    chessboard[x, y] = Instantiate(BlackTile, new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                }
            }
        }

        for (int y = 0; y < 8; y++)
        {
            for (int x = 0; x < 8; x++)
            {
                if (x%3 == y%3)
                {
                    chessboard[x, y].GetComponent<SpriteRenderer>().color = Color.green;
                }
                else if (y == 4 && x == 4)
                {
                    chessboard[x, y].GetComponent<SpriteRenderer>().color = Color.yellow;
                }
                else if ((y % 2 == 0 && x % 2 == 0) || (y % 2 == 1 && x % 2 == 1))
                {
                    chessboard[x, y].GetComponent<SpriteRenderer>().color = Color.black;
                }
                else if ((y % 2 == 0 && x % 2 == 1) || (y % 2 == 1 && x % 2 == 0))
                {
                    chessboard[x, y].GetComponent<SpriteRenderer>().color = Color.white;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
