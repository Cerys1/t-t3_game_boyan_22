using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class7 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int enemy_count = Random.Range(0, 10);
        int iterator = 0;
        do
        {
            Vector3 pos = new Vector3(Random.Range(-10, 10), Random.Range(-5, 5), 0);
            Quaternion rot = Quaternion.identity;
            GameObject.Instantiate(GameObject.Find("TemplateCube"), pos, rot);
            iterator++;
        } while (iterator < enemy_count);

        for (int y = 0; y < 8; y++)
        {
            for (int x = 0; x < 8; x++)
            {
                if (y % 2 == 0)
                {
                    if (x % 2 == 0)
                    {
                        GameObject.Instantiate(GameObject.Find("TemplateCube"), new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                    }
                }

                else if(y % 2 == 1)
                {
                    if (x % 2 == 1)
                    {
                        GameObject.Instantiate(GameObject.Find("TemplateCube"), new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
