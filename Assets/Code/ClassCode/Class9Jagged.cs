using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class9Jagged : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject WhiteTile = GameObject.Find("White");
        GameObject BlackTile = GameObject.Find("Black");
        GameObject[][] chessboard = new GameObject[8][];
        for (int i = 0; i < chessboard.Length; i++)
        {
            chessboard[i] = new GameObject[8];
        }

        for (int y = 0; y < chessboard.Length; y++)
        {
            for (int x = 0; x < chessboard[y].Length; x++)
            {
                if ((y % 2 == 0 && x % 2 == 0) || (y % 2 == 1 && x % 2 == 1))
                {
                    chessboard[y][x] = Instantiate(WhiteTile, new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                }
                else if ((y % 2 == 0 && x % 2 == 1) || (y % 2 == 1 && x % 2 == 0))
                {
                    chessboard[y][x] = Instantiate(BlackTile, new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
