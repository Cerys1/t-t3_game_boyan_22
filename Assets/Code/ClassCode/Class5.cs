using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class5 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int ble = Random.Range(0, 1000);
        if (ble < 100 || ble > 950)
        {
            Debug.Log("You lose");
        }
        else if (ble >= 101 && ble <= 950)
        {
            if (ble == 500)
            {
                Debug.Log("Bullseye");
            }
            else if (ble == 777)
            {
                Debug.Log("Jackpot");
            }
            else if((ble > 100 && ble < 250) || (ble < 950 && ble > 800))
            {
                Debug.Log("Close Call");
            }
            else
            {
                Debug.Log("You win");
            }

            string hit = Random.Range(1, 20) + 3 > 13 ? "Hit" : "Miss";
            Debug.Log(hit);
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
