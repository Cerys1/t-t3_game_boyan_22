using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class4 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float a = Mathf.Clamp(Mathf.Sin(Time.time * 3.25f) * 1.337f, -1, 5);
        GameObject.Find("MoreMath").transform.position = new Vector3(0, a, 0);
    }
}
