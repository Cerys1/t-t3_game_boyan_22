using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class8 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int[] Numbers = { 5, -7, 23, 59 };
        int sum = 0;
        for (int i = 0; i < Numbers.Length; i++)
        {
            sum += Numbers[i];
        }
        Debug.Log(sum);
        Debug.Log(Numbers[1] - Numbers[Numbers.Length - 1]);
        Numbers[0] = 42;
        Debug.Log((Numbers[0] * Numbers[1]) - (Numbers[3] / Numbers[2]));

        int[] ManyNumbers = new int[10000];
        int[] DigitCounts = new int[10];
        int[] Digits = new int[10];
        for (int i = 0; i < 10; i++)
        {
            DigitCounts[i] = 0;
            Digits[i] = i;
        }
        for (int i = 0; i < 10000; i++)
        {
            ManyNumbers[i] = Random.Range(0, 10);
            for (int j = 0; j < 10; j++)
            {
                if (ManyNumbers[i] == Digits[j])
                {
                    DigitCounts[j]++;
                }
            }
        }
        int MostCommonDigitCount = 0;
        int MostCommonDigit = 0;
        for (int i = 0; i < 10; i++)
        {
            if (MostCommonDigitCount <= DigitCounts[i])
            {
                MostCommonDigit = i;
                MostCommonDigitCount = DigitCounts[i];
            }
        }
        Debug.Log(MostCommonDigit + " with " + MostCommonDigitCount);



        int[] sorting_numbers;
        sorting_numbers = new int[6];
        for(int i = 0; i < 6; i++)
        {
            sorting_numbers[i] = (int)Random.Range(1f, 100f);
        }

        int prevnum = 0;
        int curnum = 0;
        for (int j = 0; j < 5; j++)
        {
            for (int i = 1; i < 6; i++)
            {
                prevnum = sorting_numbers[i - 1];
                curnum = sorting_numbers[i];
                if (prevnum < curnum)
                {
                    sorting_numbers[i - 1] = curnum;
                    sorting_numbers[i] = prevnum;
                }
            }
        }

        for (int i = 0; i < 6; i++)
        {
            Debug.Log(sorting_numbers[i]);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
