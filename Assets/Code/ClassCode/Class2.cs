using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        string myText;
        myText = "This is my text!";
        Debug.Log(myText);
        myText = "No, it's my text!";
        Debug.Log(myText);

        string otherText = myText;
        Debug.Log(otherText);

        float pi = 3.14f;
        int Leopard = 2;
        double LeopardMBT = Leopard;
        Debug.Log(pi);
        Debug.Log(Leopard);
        Debug.Log(LeopardMBT);
        int pie = (int)pi;
        string floatIntoString = pi.ToString();
        Debug.Log(pie);
        Debug.Log(floatIntoString);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
