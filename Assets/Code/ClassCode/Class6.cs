﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class6 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        for(float i = 0; i<5; i +=1.5f)
        {
            Vector3 pos = new Vector3(i, 0, 0);
            Quaternion rot = Quaternion.identity;
            GameObject.Instantiate(GameObject.Find("TemplateCube"), pos, rot);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
